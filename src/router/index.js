import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Chat from '../views/Chat.vue'
import Finquest from '../views/Finquest.vue'
import FinquestNavigator from '../views/FinNavigator.vue'
import FinquestRegulator from '../views/FinRegulator.vue'
import FinquestWelcome from '../components/finquest/FinquestWelcome.vue'
import FinquestRules from '../components/finquest/FinquestRules.vue'
import FinquestMapPoints from '../components/finquest/FinquestMapPoints.vue'
import FinquestRegulatorWelcome from '../components/fin-regulator/FinquestRegulatorWelcome.vue'
import FinquestRegulatorRules from '../components/fin-regulator/FinquestRegulatorRules.vue'
import FinquestRegulatorMap from '../components/fin-regulator/FinRegulatorMap.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/chat',
    name: 'chat',
    component: Chat
  },
  {
    path: '/finquest',
    name: 'finquest',
    component: Finquest,
    children: [
      {
        path: '/',
        name: 'finquest-welcome',
        component: FinquestWelcome
      },
      {
        path: '/finquest/rules',
        name: 'finquest-rules',
        component: FinquestRules
      }
    ]
  },
  {
    path: '/finquest-map-points',
    name: 'finquest-map-points',
    component: FinquestMapPoints
  },
  {
    path: '/fin-regulator',
    name: 'finquest-regulator',
    component: FinquestRegulator,
    children: [
      {
        path: '/',
        name: 'finquest-regulator-welcome',
        component: FinquestRegulatorWelcome
      },
      {
        path: '/fin-regulator/rules',
        name: 'finquest-regulator-rules',
        component: FinquestRegulatorRules
      }
    ]
  },
  {
    path: '/finquest-regulator-map',
    name: 'finquest-regulator-game',
    component: FinquestRegulatorMap
  },
  {
    path: '/fin-navigator',
    name: 'finquest-navigator',
    component: FinquestNavigator
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
