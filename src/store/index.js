import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pointOpen: false,
    hintText: null
  },
  mutations: {
    changePointStatus(state, value) {
      state.pointOpen = value;
    },
    changeHintText(state, value) {
      state.hintText = value;
    },
  },
  actions: {
    initChangePoint(context, value) {
      context.commit('changePointStatus', value);
    },
    initHintText(context, value) {
      context.commit('changeHintText', value);
    }
  },
  getters: {
    getPointStatus: state => {
      return state.pointOpen;
    },
    getHintText: state => {
      return state.hintText;
    }
  }
})
